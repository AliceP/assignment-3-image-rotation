#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdlib.h>

#define PIXEL_SIZE sizeof(struct pixel)

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum copy_status {
    COPY_OK = 0,
    COPY_ERROR,
};

size_t pixel_ptr(struct image const* img, uint32_t row, uint32_t col);

struct image image_create(uint64_t width, uint64_t height);

void image_destroy(struct image img);

enum copy_status
image_copy(struct image const* source_img, struct image* target_img);

#endif
