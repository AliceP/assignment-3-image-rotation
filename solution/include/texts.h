#ifndef TEXTS_H
#define TEXTS_H

#include "bmp.h"

static char const* TEXT_USAGE =
    "Usage: ./image-transformer <source-image> <transformed-image> <angle>\n"
    "\tSupported angles: 0, 90, -90, 180, -180, 270, -270\n";

static char const* TEXT_FILE_OPEN_ERROR = "Cannot open file.\n";

static const char* TEXT_READ_ERROR[] = {
    [READ_ERROR] = "Something went wrong during image reading\n",
    [READ_INVALID_HEADER] = "Invalid file header.\n",
    [READ_INVALID_SIGNATURE] = "Unsupported file format.\n",
    [READ_INVALID_BITS] = "File possibly corrupted.\n",
};

static const char* TEXT_WRITE_ERROR[] = {
    [WRITE_ERROR] = "Something went wrong during image writing.\n",
};

static const char* TEXT_FAIL_ROTATION = "Cannot rotate image.\n";
#endif
