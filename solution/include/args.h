#ifndef ARGS_H
#define ARGS_H

#include <errno.h>
#include <stdint.h>
#include <stdlib.h>

enum parse_args_status {
    ARGS_OK = 0,
    ARGS_WRONG_NUMBER,
    ARGS_BAD_ANGLE,
    ARGS_ERROR,
};

struct args {
    char const* source_path;
    char const* target_path;
    int16_t angle;
};

enum parse_args_status parse_args(int argc, char** argv, struct args* args);

#endif
