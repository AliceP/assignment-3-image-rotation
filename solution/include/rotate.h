#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"

enum rotate_status {
    ROTATE_OK = 0,
    ROTATE_ERROR = 80,
    CREATE_ROTATED_IMG_ERROR = 81,
};

enum rotate_status image_rotate(struct image const *source_img, struct image *target_img, int16_t angle);

#endif
