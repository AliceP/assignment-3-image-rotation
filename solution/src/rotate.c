#include "rotate.h"

#include <stdint.h>
#include <stdlib.h>

#include "image.h"


static enum rotate_status create_rotated_image(
    struct image *target_img,
    uint32_t new_width,
    uint32_t new_height
) {
    *target_img = image_create(new_width, new_height);
    if (!target_img->data) {
        return CREATE_ROTATED_IMG_ERROR;
    }

    return ROTATE_OK;
}

static enum rotate_status
image_rotate_90(struct image const *source_img, struct image *target_img) {
    enum rotate_status status =
        create_rotated_image(target_img, source_img->height, source_img->width);
    if (status != ROTATE_OK) {
        return status;
    }

    for (uint32_t row = 0; row < source_img->height; row++) {
        for (uint32_t col = 0; col < source_img->width; col++) {
            size_t source_pixel =
                pixel_ptr(source_img, row, source_img->width - col - 1);
            size_t target_pixel = pixel_ptr(target_img, col, row);
            target_img->data[target_pixel] = source_img->data[source_pixel];
        }
    }
    return ROTATE_OK;
}

static enum rotate_status
image_rotate_180(struct image const *source_img, struct image *target_img) {
    enum rotate_status status =
        create_rotated_image(target_img, source_img->width, source_img->height);
    if (status != ROTATE_OK) {
        return status;
    }

    for (uint32_t row = 0; row < source_img->height; row++) {
        for (uint32_t col = 0; col < source_img->width; col++) {
            size_t source_pixel = pixel_ptr(
                source_img,
                source_img->height - row - 1,
                source_img->width - col - 1
            );
            size_t target_pixel = pixel_ptr(target_img, row, col);
            target_img->data[target_pixel] = source_img->data[source_pixel];
        }
    }
    return ROTATE_OK;
}

static enum rotate_status
image_rotate_270(struct image const *source_img, struct image *target_img) {
    enum rotate_status status =
        create_rotated_image(target_img, source_img->height, source_img->width);
    if (status != ROTATE_OK) {
        return status;
    }

    for (uint32_t row = 0; row < source_img->height; row++) {
        for (uint32_t col = 0; col < source_img->width; col++) {
            size_t source_pixel =
                pixel_ptr(source_img, source_img->height - row - 1, col);
            size_t target_pixel = pixel_ptr(target_img, col, row);
            target_img->data[target_pixel] = source_img->data[source_pixel];
        }
    }
    return ROTATE_OK;
}

enum rotate_status image_rotate(
    struct image const *source_img,
    struct image *target_img,
    int16_t angle
) {
    switch (angle) {
        case 0:
            return (enum rotate_status)image_copy(source_img, target_img);
        case 90:
        case -270:
            return image_rotate_90(source_img, target_img);
        case 180:
        case -180:
            return image_rotate_180(source_img, target_img);
        case 270:
        case -90:
            return image_rotate_270(source_img, target_img);
        default:
            return ROTATE_ERROR;
    }
}
