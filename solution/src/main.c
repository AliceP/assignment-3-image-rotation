#include <errno.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "args.h"
#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "texts.h"

#define RESOURCES source_file, target_file, source_img, target_img

static void cleanup(
    FILE* source_file,
    FILE* target_file,
    struct image source_img,
    struct image target_img
) {
    if (source_file) fclose(source_file);
    if (target_file) fclose(target_file);
    image_destroy(source_img);
    image_destroy(target_img);
}

static void exit_and_cleanup(
    int code,
    char const* msg,
    FILE* source_file,
    FILE* target_file,
    struct image source_img,
    struct image target_img
) {
    cleanup(RESOURCES);
    if (msg) fputs(msg, stderr);
    exit(code);
}

int main(int argc, char** argv) {
    FILE* source_file = NULL;
    FILE* target_file = NULL;
    struct image source_img = {0};
    struct image target_img = {0};

    struct args args = {0};
    if (parse_args(argc, argv, &args) != ARGS_OK)
        exit_and_cleanup(1, TEXT_USAGE, RESOURCES);

    source_file = fopen(args.source_path, "rb");
    if (!source_file) exit_and_cleanup(1, TEXT_FILE_OPEN_ERROR, RESOURCES);

    enum read_status read_status = from_bmp(source_file, &source_img);
    if (read_status != READ_OK)
        exit_and_cleanup(1, TEXT_READ_ERROR[read_status], RESOURCES);

    enum rotate_status rotate_status =
        image_rotate(&source_img, &target_img, args.angle);
    if (rotate_status != ROTATE_OK)
        exit_and_cleanup(1, TEXT_FAIL_ROTATION, RESOURCES);

    target_file = fopen(args.target_path, "wb");
    if (!target_file) exit_and_cleanup(1, TEXT_FILE_OPEN_ERROR, RESOURCES);

    enum write_status write_status = to_bmp(target_file, &target_img);
    if (write_status != WRITE_OK)
        exit_and_cleanup(1, TEXT_WRITE_ERROR[write_status], RESOURCES);

    cleanup(RESOURCES);
    return 0;
}
