#include "image.h"

#include <stdio.h>
#include <stdlib.h>

size_t pixel_ptr(struct image const* img, uint32_t row, uint32_t col) {
    return row * img->width + col;
}

struct image image_create(uint64_t width, uint64_t height) {
    struct pixel* data = malloc(width * height * PIXEL_SIZE);
    return (struct image){
        .height = height,
        .width = width,
        .data = data,
    };
}

void image_destroy(struct image img) {
        free(img.data);
}

enum copy_status
image_copy(struct image const* source_img, struct image* target_img) {
    *target_img = image_create(source_img->width, source_img->height);
    if (!target_img->data) return COPY_ERROR;

    for (uint32_t row = 0; row < source_img->height; row++) {
        for (uint32_t col = 0; col < source_img->width; col++) {
            size_t pixel = pixel_ptr(source_img, row, col);
            target_img->data[pixel] = source_img->data[pixel];
        }
    }
    return COPY_OK;
}
