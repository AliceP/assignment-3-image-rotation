#include "args.h"

int is_valid_angle(long angle) {
    return (angle >= -270 && angle <= 270 && angle % 90 == 0 && errno != EINVAL);
}

enum parse_args_status parse_args(int argc, char** argv, struct args* args) {
    if (!args) return ARGS_ERROR;
    if (argc != 4) {
        return ARGS_WRONG_NUMBER;
    }
    long angle = strtol(argv[3], NULL, 10);
    if (!is_valid_angle(angle)) {
        return ARGS_BAD_ANGLE;
    }
    *args = (struct args
    ){.angle = (int16_t)angle, .source_path = argv[1], .target_path = argv[2]};
    return ARGS_OK;
}
