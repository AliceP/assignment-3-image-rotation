#include "bmp.h"

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "image.h"

const size_t BMP_HEADER_SIZE = sizeof(struct bmp_header);
const uint16_t BMP_FILE_TYPE = 0x4D42;
const uint16_t BMP_BI_SIZE = 40;
const uint32_t BMP_PIXELS_PER_METER = 2834;
const uint16_t BMP_PLANES = 1;

const int64_t MODULE_OF_DIVISION = 4;


static void init_default_bmp_header(struct bmp_header* header, uint32_t width, uint32_t height) {
    *header = (struct bmp_header) {
        .bfType = BMP_FILE_TYPE,
        .bfFileSize = 0,  // Это поле будет заполнено позже при записи в файл
        .bfReserved = 0,
        .bfOffBits = BMP_HEADER_SIZE,
        .biSize = BMP_BI_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = BMP_PLANES,
        .biBitCount = PIXEL_SIZE * 8,
        .biCompression = 0,  // Дополнительные поля могут быть добавлены при необходимости
        .biSizeImage = 0,    // Это поле также будет заполнено позже при записи в файл
        .biXPelsPerMeter = BMP_PIXELS_PER_METER,
        .biYPelsPerMeter = BMP_PIXELS_PER_METER,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

static void debug_header(struct bmp_header const* header) {
    printf(
        "{\n"
        "\tbfType: %" PRIu16
        "\n"
        "\tbfFileSize: %" PRIu32
        "\n"
        "\tbfReserved: %" PRIu32
        "\n"
        "\tbfOffBits: %" PRIu32
        "\n"
        "\tbiSize: %" PRIu32
        "\n"
        "\tbiWidth: %" PRIu32
        "\n"
        "\tbiHeight: %" PRIu32
        "\n"
        "\tbiPlanes: %" PRIu16
        "\n"
        "\tbiBitCount: %" PRIu16
        "\n"
        "\tbiCompression: %" PRIu32
        "\n"
        "\tbiSizeImage: %" PRIu32
        "\n"
        "\tbiXPelsPerMeter: %" PRIu32
        "\n"
        "\tbiYPelsPerMeter: %" PRIu32
        "\n"
        "\tbiClrUsed: %" PRIu32
        "\n"
        "\tbiClrImportant: %" PRIu32
        "\n"
        "}\n",
        header->bfType,
        header->bfFileSize,
        header->bfReserved,
        header->bfOffBits,
        header->biSize,
        header->biWidth,
        header->biHeight,
        header->biPlanes,
        header->biBitCount,
        header->biCompression,
        header->biSizeImage,
        header->biXPelsPerMeter,
        header->biYPelsPerMeter,
        header->biClrUsed,
        header->biClrImportant
    );
}

static uint32_t get_padding(uint32_t width) {
    return MODULE_OF_DIVISION - (width * PIXEL_SIZE % MODULE_OF_DIVISION);
}

enum read_status static read_header(FILE* in, struct bmp_header* header) {
    if (fread(header, BMP_HEADER_SIZE, 1, in) != 1) return READ_INVALID_HEADER;
    if (header->bfType != BMP_FILE_TYPE) return READ_INVALID_SIGNATURE;
    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    enum read_status header_status = read_header(in, &header);
    if (header_status != READ_OK) return header_status;

    debug_header(&header);

    *img = image_create(header.biWidth, header.biHeight);
    if (!img->data) return READ_ERROR;

    const uint8_t padding = get_padding(header.biWidth);

    if (fseek(in, header.bfOffBits, SEEK_SET) != 0) return READ_INVALID_BITS;
    size_t n_read;
    struct pixel* row_data = img->data;
    for (uint32_t row = 0; row < header.biHeight; row++) {
        n_read = fread(row_data, PIXEL_SIZE, header.biWidth, in);
        n_read += fseek(in, padding, SEEK_CUR);
        if (n_read != header.biWidth) {
            image_destroy(*img);
            return READ_INVALID_BITS;
        }
        row_data += header.biWidth;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    if (!img) return WRITE_ERROR;

    const uint8_t padding = get_padding(img->width);
    const size_t file_size = BMP_HEADER_SIZE + (img->width * PIXEL_SIZE + padding) * img->height;

    struct bmp_header header;
    init_default_bmp_header(&header, img->width, img->height);
    header.bfFileSize = file_size;
    header.biSizeImage = (img->width * PIXEL_SIZE + padding) * img->height;

    debug_header(&header);

    size_t n_write = fwrite(&header, BMP_HEADER_SIZE, 1, out);
    if (n_write != 1) return WRITE_ERROR;

    const char padding_bytes[3] = {0};
    struct pixel* row_data = img->data;
    for (uint32_t row = 0; row < header.biHeight; row++) {
        n_write = fwrite(row_data, PIXEL_SIZE, header.biWidth, out);
        n_write += fwrite(padding_bytes, 1, padding, out);
        if (n_write != header.biWidth + padding) return WRITE_ERROR;
        row_data += header.biWidth;
    }
    return WRITE_OK;
}
